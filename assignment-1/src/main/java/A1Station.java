import java.util.Scanner;
public class A1Station {
	private static final double THRESHOLD = 250; // in kilograms
	TrainCar next;
	static double totalWeight = 0;
	// You can add new variables or methods in this class
	static String category = "";
	static double realMassIndex;
	public static void main(String[] args) {
		// TODO Complete me!
		Scanner input = new Scanner(System.in);
		int cats = input.nextInt();
		WildCat[] listCats= new WildCat[cats];
		for(int i = 0;i < cats; i++) {
			String namaCats = input.next();
			String[] namaCatsSplit = namaCats.split(",");
			listCats[i] = new WildCat(namaCatsSplit[0],Double.parseDouble(namaCatsSplit[1]),Double.parseDouble(namaCatsSplit[2]));
		}
		TrainCar[] listCars = new TrainCar[cats];
		for(int i = 0; i < cats; i++) {
			listCars[i] = new TrainCar(listCats[i]);
		}
		totalWeight = listCars[0].cat.weight;
		for(int i = 0; i < cats-1; i++) {
			if(totalWeight + listCars[i].cat.weight > THRESHOLD) {
				realMassIndex = listCars[i].computeTotalMassIndex()/TrainCar.count;
				totalWeight = 0;
				System.out.println("Train departs to Javari Park");
				System.out.print("[LOCO]<-");
				listCars[i].printCar();
				System.out.printf("Average mass index of all cats: %.2f \n",realMassIndex);
				if(realMassIndex < 18.5) {
					category = "underweight";
				}
				else if(realMassIndex >= 18.5 && realMassIndex < 25) {
					category = "normal";
				}
				else if(realMassIndex >= 25 && realMassIndex < 30) {
					category = "overweight";
				}
				else if(realMassIndex >= 30) {
					category = "obese";
				}
				System.out.println("In average, the cats in the train are *"+category+"*\n");
				TrainCar.count = 0;
				continue;
			}
			else if(totalWeight + listCars[i].cat.weight <= THRESHOLD) {
				totalWeight += listCars[i].cat.weight;
				listCars[i+1].setNext(listCars[i]);
			}	
		}
		realMassIndex = listCars[cats-1].computeTotalMassIndex()/TrainCar.count;
		System.out.println("Train departs to Javari Park");
		System.out.print("[LOCO]<-");
		listCars[cats-1].printCar();
		System.out.printf("Average mass index of all cats: %.2f \n",realMassIndex);
		if(realMassIndex < 18.5) {
			category = "underweight";
		}
		else if(18.5 <= realMassIndex && realMassIndex< 25) {
			category = "normal";
		}
		else if(25 <= realMassIndex && realMassIndex < 30) {
			category = "overweight";
		}
		else if(realMassIndex >= 30) {
			category = "obese";
		}
		System.out.println("In average, the cats in the train are *"+category+"*");
	}
}