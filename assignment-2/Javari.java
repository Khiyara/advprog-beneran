import java.util.Scanner;
import java.util.ArrayList;
public class Javari {
    static Scanner input = new Scanner(System.in);
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesIn = new ArrayList<ArrayList<ArrayList<Cage>>>();
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesInCopy = new ArrayList<ArrayList<ArrayList<Cage>>>();
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesOut = new ArrayList<ArrayList<ArrayList<Cage>>>();
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesOutCopy = new ArrayList<ArrayList<ArrayList<Cage>>>();
    static Animal[][] animalCats;
    static Animal[][] animalParrot;
    static Animal[][] animalHamster;
    static Animal[][] animalEagles;
    static Animal[][] animalLion;

    public static void main(String [] args) {
        for(int i=0; i<3; i++) {
            cagesIn.add(new ArrayList<ArrayList<Cage>>());
            cagesInCopy.add(new ArrayList<ArrayList<Cage>>());
        }
        for(int i=0; i<2; i++) {
            cagesOut.add(new ArrayList<ArrayList<Cage>>());
            cagesOutCopy.add(new ArrayList<ArrayList<Cage>>());
        }
        System.out.println("Welcome to Javari Park!\nInput the number of animals");
        System.out.print("cat: ");
        int cats = input.nextInt();
        Animal[][] animalCats = new Animal[3][cats];
        ArrangeCage.setCageIn(animalCats, cats, "cat", 0, cagesIn, cagesInCopy, "INDOOR");

        System.out.print("lion: ");
        int lions = input.nextInt();
        Animal[][] animalLion = new Animal[3][lions];
        ArrangeCage.setCageIn(animalLion, lions, "lion", 0, cagesOut, cagesOutCopy, "OUTDOOR");

        System.out.print("eagle: ");
        int eagles = input.nextInt();
        Animal[][] animalEagles = new Animal[3][eagles];
        ArrangeCage.setCageIn(animalEagles, eagles, "eagle", 1, cagesOut, cagesOutCopy, "OUTDOOR");

        System.out.print("parrot: ");
        int parrots = input.nextInt();
        Animal[][] animalParrot = new Animal[3][parrots];
        ArrangeCage.setCageIn(animalParrot, parrots, "parrot", 1, cagesIn, cagesInCopy, "INDOOR");

        System.out.print("hamster: ");
        int hamsters = input.nextInt();
        Animal[][] animalHamster = new Animal[3][hamsters];
        ArrangeCage.setCageIn(animalHamster, hamsters, "hamster",2, cagesIn, cagesInCopy, "INDOOR");
        
        System.out.println("Cage Arrangement:");
        if (cats>0){
            ArrangeCage.ArrangeCage(cagesIn, cagesInCopy, 0, "Indoor"); 
        }
        if (lions>0){
            ArrangeCage.ArrangeCage(cagesOut, cagesOutCopy, 0, "Outdoor");
        }
        if (eagles>0){
            ArrangeCage.ArrangeCage(cagesOut, cagesOutCopy, 1, "Outdoor");
        }
        if (parrots>0){
            ArrangeCage.ArrangeCage(cagesIn, cagesInCopy, 1, "Indoor"); 
        }
        if (hamsters>0){
            ArrangeCage.ArrangeCage(cagesIn, cagesInCopy, 2, "Indoor");
        }
        while (true) {
            System.out.println("Which animal you want to visit?\n(1:Cat,2:Eagle,3:Hamster,4:Parrot,5:Lion,99:exit)");
            int x = input.nextInt();
            boolean ada = false;
            if (x==1) {
                System.out.print("Mention the name of cat you want to visit: ");
                String nama = input.next();
                for(int i=0;i<cagesIn.get(0).size();i++) {
                    for (int j=0;j<cagesIn.get(0).get(i).size();j++) {           
                        if(nama.equals(cagesIn.get(0).get(i).get(j).animals.getNama())) {
                            ada = true;
                            System.out.println("You are visiting "+nama+" (cat) now, what would you like to do?");
                            System.out.println("1: Brush the fur 2: Cuddle");
                            int command = input.nextInt();
                            cagesIn.get(0).get(i).get(j).animals.doSomething(command);
                        } 
                    } 
                } if (!ada){
                    System.out.println("There is no cat with that name! Back to the office!\n");
                }   
            } else if(x==2) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String nama = input.next();
                for(int i=0;i<cagesOut.get(1).size();i++) {
                    for (int j=0;j<cagesOut.get(1).get(i).size();j++) {           
                        if(nama.equals(cagesOut.get(1).get(i).get(j).animals.getNama())) {
                            ada = true;
                            System.out.println("You are visiting "+nama+" (eagle) now, what would you like to do?");
                            System.out.println("1: Order to fly");
                            int command = input.nextInt();
                            cagesOut.get(1).get(i).get(j).animals.doSomething(command);
                        } 
                    } 
                }  if (!ada){
                    System.out.println("There is no eagle with that name! Back to the office!\n");
                }
            } else if(x==3) {
                System.out.print("Mention the name of hamster you want to visit: ");
                String nama = input.next();
                for(int i=0;i<cagesIn.get(2).size();i++) {
                    for (int j=0;j<cagesIn.get(2).get(i).size();j++) {           
                        if(nama.equals(cagesIn.get(2).get(i).get(j).animals.getNama())) {
                            ada = true;
                            System.out.println("You are visiting "+nama+" (hamster) now, what would you like to do?");
                            System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                            int command = input.nextInt();
                            cagesIn.get(2).get(i).get(j).animals.doSomething(command);
                        } 
                    } 
                }  if (!ada){
                    System.out.println("There is no hamster with that name! Back to the office!\n");
                }
            } else if(x==4) {
                System.out.print("Mention the name of parrot you want to visit: ");
                String nama = input.next();
                for(int i=0;i<cagesIn.get(1).size();i++) {
                    for (int j=0;j<cagesIn.get(1).get(i).size();j++) {           
                        if(nama.equals(cagesIn.get(1).get(i).get(j).animals.getNama())) {
                            ada = true;
                            System.out.println("You are visiting "+nama+" (parrot) now, what would you like to do?");
                            System.out.println("1: Order to fly 2: Do conversation");
                            int command = input.nextInt();
                            if (command==2){
                                System.out.print("You say:");
                                input.nextLine();
                                String kata = input.nextLine();
                                cagesIn.get(1).get(i).get(j).animals.doSomething(command,kata);
                                continue;
                            }
                            cagesIn.get(1).get(i).get(j).animals.doSomething(command);
                        } 
                    } 
                }  if (!ada){
                    System.out.println("There is no parrot with that name! Back to the office!\n");
                }
            } else if(x==5) {
                System.out.print("Mention the name of lion you want to visit: ");
                String nama = input.next();
                for(int i=0;i<cagesOut.get(0).size();i++) {
                    for (int j=0;j<cagesOut.get(0).get(i).size();j++) {           
                        if(nama.equals(cagesOut.get(0).get(i).get(j).animals.getNama())) {
                            ada = true;
                            System.out.println("You are visiting "+nama+" (lion) now, what would you like to do?");
                            System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                            int command = input.nextInt();
                            cagesOut.get(0).get(i).get(j).animals.doSomething(command);
                        } 
                    } 
                }  if (!ada){
                    System.out.println("There is no lion with that name! Back to the office!\n");
                }
            } else if(x==99) {
                break;
            } else{
                System.out.println("You do nothing");
                continue;
            }
        }
    }
}//A|30,B|30,C|30,D|30,E|30,F|30,G|30,H|30,I|30,J|30