package Animal;
public class Animal {
    private String nama;
    private int length;
    private String kategori;

    public Animal(String nama, int length, String kategori) {
        this.nama = nama;
        this.length = length;     
        this.kategori = kategori;
    }
    public void brushed() {
        System.out.println("This animal can't be brushed");
     }
    public void fly() {
        System.out.println("This animal can't fly");
    }     
    public String getNama() {
        return this.nama;
    }
    public int getLength() {
        return this.length;
    }
    public String getKategori(){
        return this.kategori;
    }
}