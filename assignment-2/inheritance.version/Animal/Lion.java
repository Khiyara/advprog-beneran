package Animal;
import Animal.Animal;
public class Lion extends Animal {
    public Lion(String nama, int length, String kategori){
        super(nama, length, kategori);
    }
    public void hunting() {
        System.out.println("Lion is hunting..");
        System.out.println(getNama()+" makes a voice: err!!");
        System.out.println("Back to the office!");
    }
    public void brushed() {
        System.out.println("Clean the lion's mane..");
        System.out.println(getNama()+" makes a voice:Hauhhmm!");
        System.out.println("Back to the office!");
    }
    public void disturbed() {
        System.out.println(getNama()+" makes a voice:HAUHHMMM!");
        System.out.println("Back to the office!");
    }
}