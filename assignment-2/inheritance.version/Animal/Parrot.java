package Animal;
import Animal.Animal;
public class Parrot extends Animal {
    public Parrot(String nama, int length, String kategori){
        super(nama, length, kategori);
    }
    public void fly() {
        System.out.println("Parrot "+getNama()+" flies!");
        System.out.println(getNama()+" makes a voice:FLYYYYYY!!");
        System.out.println("Back to the office!");
    }
    public void speak(String kata) {
        System.out.println(getNama()+" says:"+kata.toUpperCase());
        System.out.println("Back to the office!");
    }
}