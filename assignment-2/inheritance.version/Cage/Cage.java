package Cage;
import Animal.Animal;
public class Cage {
    public static final int INDOOR_A = 45;
    public static final int INDOOR_B = 60;
    public static final int OUTDOOR_A = 75;
    public static final int OUTDOOR_B = 90;
    private Animal animals;
    private char level;
    
    public Cage(Animal animals) {
        this.animals = animals;
        if (animals.getKategori().equals("INDOOR")){
             if(this.animals.getLength() < INDOOR_A) {
                this.level = 'A';
            } else if(this.animals.getLength() >= INDOOR_A && this.animals.getLength() < INDOOR_B) {
                this.level = 'B';
            } else {
                this.level = 'C';
            } 
        }
        else {
             if(this.animals.getLength() < OUTDOOR_A) {
                this.level = 'A';
            } else if(this.animals.getLength() >= OUTDOOR_A && this.animals.getLength() < OUTDOOR_B) {
                this.level = 'B';
            } else {
                this.level = 'C';
            }
        }
    }
    public char getType() {
        return this.level;
    }
    public Animal getAnimal() {
        return this.animals;
    }
}