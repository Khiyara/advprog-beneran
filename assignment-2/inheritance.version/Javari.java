import java.util.Scanner;
import java.util.ArrayList;
import Animal.*;
import Cage.*;
public class Javari {
    static Scanner input = new Scanner(System.in);
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesIn = new ArrayList<ArrayList<ArrayList<Cage>>>();
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesInCopy = new ArrayList<ArrayList<ArrayList<Cage>>>();
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesOut = new ArrayList<ArrayList<ArrayList<Cage>>>();
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesOutCopy = new ArrayList<ArrayList<ArrayList<Cage>>>();
    static Animal[][] animalCats;
    static Animal[][] animalParrot;
    static Animal[][] animalHamster;
    static Animal[][] animalEagles;
    static Animal[][] animalLion; loadJSON();
    
    public static void main(String [] args) {
        for(int i=0; i<3; i++) {
            cagesIn.add(new ArrayList<ArrayList<Cage>>());
            cagesInCopy.add(new ArrayList<ArrayList<Cage>>());
        }
        for(int i=0; i<2; i++) {
            cagesOut.add(new ArrayList<ArrayList<Cage>>());
            cagesOutCopy.add(new ArrayList<ArrayList<Cage>>());
        }
        System.out.println("Welcome to Javari Park!\nInput the number of animals");
        System.out.print("cat: ");
        try {
            try {
                int cats = input.nextInt();
            Animal[][] animalCats = new Animal[3][cats];
            Arrange.setCageIn(animalCats, cats, "cat", 0, cagesIn, cagesInCopy, "INDOOR");

            System.out.print("lion: ");
            int lions = input.nextInt();
            Animal[][] animalLion = new Animal[3][lions];
            Arrange.setCageIn(animalLion, lions, "lion", 0, cagesOut, cagesOutCopy, "OUTDOOR");

            System.out.print("eagle: ");
            int eagles = input.nextInt();
            Animal[][] animalEagles = new Animal[3][eagles];
            Arrange.setCageIn(animalEagles, eagles, "eagle", 1, cagesOut, cagesOutCopy, "OUTDOOR");

            System.out.print("parrot: ");
            int parrots = input.nextInt();
            Animal[][] animalParrot = new Animal[3][parrots];
            Arrange.setCageIn(animalParrot, parrots, "parrot", 1, cagesIn, cagesInCopy, "INDOOR");

            System.out.print("hamster: ");
            int hamsters = input.nextInt();
            Animal[][] animalHamster = new Animal[3][hamsters];
            Arrange.setCageIn(animalHamster, hamsters, "hamster",2, cagesIn, cagesInCopy, "INDOOR");
            System.out.println("Cage Arrangement:");
            if (cats>0) Arrange.ArrangeCage(cagesIn, cagesInCopy, 0, "Indoor");         
            if (lions>0) Arrange.ArrangeCage(cagesOut, cagesOutCopy, 0, "Outdoor");        
            if (eagles>0) Arrange.ArrangeCage(cagesOut, cagesOutCopy, 1, "Outdoor");        
            if (parrots>0) Arrange.ArrangeCage(cagesIn, cagesInCopy, 1, "Indoor"); 
            if (hamsters>0) Arrange.ArrangeCage(cagesIn, cagesInCopy, 2, "Indoor");  
            } catch (Exception E) {
                System.out.println("Please use the right format");
                System.exit(0);
            }
            while (true) {
                System.out.println("Which animal you want to visit?\n(1:Cat,2:Eagle,3:Hamster,4:Parrot,5:Lion,99:exit)");
                int x = input.nextInt();
                boolean ada = false;
                if (x==1) {
                    System.out.print("Mention the name of cat you want to visit: ");
                    String nama = input.next();
                    for(int i=0;i<cagesIn.get(0).size();i++) {
                        for (int j=0;j<cagesIn.get(0).get(i).size();j++) {           
                            if(nama.equals(cagesIn.get(0).get(i).get(j).getAnimal().getNama())) {
                                ada = true;
                                System.out.println("You are visiting "+nama+" (cat) now, what would you like to do?");
                                System.out.println("1: Brush the fur 2: Cuddle");
                                int command = input.nextInt();
                                if (command==1) (cagesIn.get(0).get(i).get(j).getAnimal()).brushed();
                                else if (command==2) ((Cat)cagesIn.get(0).get(i).get(j).getAnimal()).cuddled();
                                else { System.out.println("You do nothing!\nBack to the office!"); }                           
                            } 
                        } 
                    } if (!ada){
                        System.out.println("There is no cat with that name! Back to the office!\n");
                    }   
                } else if(x==2) {
                    System.out.print("Mention the name of eagle you want to visit: ");
                    String nama = input.next();
                    for(int i=0;i<cagesOut.get(1).size();i++) {
                        for (int j=0;j<cagesOut.get(1).get(i).size();j++) {           
                            if(nama.equals(cagesOut.get(1).get(i).get(j).getAnimal().getNama())) {
                                ada = true;
                                System.out.println("You are visiting "+nama+" (eagle) now, what would you like to do?");
                                System.out.println("1: Order to fly");
                                int command = input.nextInt();
                                if (command==1) (cagesOut.get(1).get(i).get(j).getAnimal()).fly();
                                else {System.out.println("You do nothing!\nBack to the office!"); }
                            } 
                        } 
                    }  if (!ada){
                        System.out.println("There is no eagle with that name! Back to the office!\n");
                    }
                } else if(x==3) {
                    System.out.print("Mention the name of hamster you want to visit: ");
                    String nama = input.next();
                    for(int i=0;i<cagesIn.get(2).size();i++) {
                        for (int j=0;j<cagesIn.get(2).get(i).size();j++) {           
                            if(nama.equals(cagesIn.get(2).get(i).get(j).getAnimal().getNama())) {
                                ada = true;
                                System.out.println("You are visiting "+nama+" (hamster) now, what would you like to do?");
                                System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                                int command = input.nextInt();
                                if (command==1) ((Hamster)cagesIn.get(2).get(i).get(j).getAnimal()).gnawing();
                                else if (command==2) ((Hamster)cagesIn.get(2).get(i).get(j).getAnimal()).run();
                                else {System.out.println("You do nothing!\nBack to the office!"); }
                            } 
                        } 
                    }  if (!ada){
                        System.out.println("There is no hamster with that name! Back to the office!\n");
                    }
                } else if(x==4) {
                    System.out.print("Mention the name of parrot you want to visit: ");
                    String nama = input.next();
                    for(int i=0;i<cagesIn.get(1).size();i++) {
                        for (int j=0;j<cagesIn.get(1).get(i).size();j++) {           
                            if(nama.equals(cagesIn.get(1).get(i).get(j).getAnimal().getNama())) {
                                ada = true;
                                System.out.println("You are visiting "+nama+" (parrot) now, what would you like to do?");
                                System.out.println("1: Order to fly 2: Do conversation");
                                int command = input.nextInt();
                                if (command==2){
                                    System.out.print("You say:");
                                    input.nextLine();
                                    String kata = input.nextLine();
                                    ((Parrot)cagesIn.get(1).get(i).get(j).getAnimal()).speak(kata);
                                } else if (command==1) (cagesIn.get(1).get(i).get(j).getAnimal()).fly();
                                else {System.out.println("You do nothing!\nBack to the office!"); }                    
                            } 
                        } 
                    }  if (!ada){
                        System.out.println("There is no parrot with that name! Back to the office!\n");
                    }
                } else if(x==5) {
                    System.out.print("Mention the name of lion you want to visit: ");
                    String nama = input.next();
                    for(int i=0;i<cagesOut.get(0).size();i++) {
                        for (int j=0;j<cagesOut.get(0).get(i).size();j++) {           
                            if(nama.equals(cagesOut.get(0).get(i).get(j).getAnimal().getNama())) {
                                ada = true;
                                System.out.println("You are visiting "+nama+" (lion) now, what would you like to do?");
                                System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                                int command = input.nextInt();
                                if (command == 1) ((Lion)cagesOut.get(0).get(i).get(j).getAnimal()).hunting();
                                else if (command == 2) (cagesOut.get(0).get(i).get(j).getAnimal()).brushed();
                                else if (command == 3) ((Lion)cagesOut.get(0).get(i).get(j).getAnimal()).disturbed();
                                else {System.out.println("You do nothing!\nBack to the office!");}      
                            } 
                        } 
                    }  if (!ada){
                        System.out.println("There is no lion with that name! Back to the office!\n");
                    }
                } else if(x==99) {
                    break;
                } else{
                    System.out.println("You do nothing");
                    continue;
                }
            } 
        } catch (Exception input) {
            System.out.println("You should input integer"); 
        }    
    }
}