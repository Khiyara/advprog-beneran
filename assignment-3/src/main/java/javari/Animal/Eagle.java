package Animal;
import Animal.Animal;
public class Eagle extends Aves {
    private String specialStatus;
    
    public Eagle(String nama, int id, double length, double weight, String gender, String type, String health){
        super(nama, id, length, weight, gender, type, health);
        this.specialStatus = "";
    }
    public Eagle(String nama, int id, double length, double weight, String gender, String type,String specialStatus, String health){
        super(nama, id, length, weight, gender, type, specialStatus, health);
    }
    public boolean circleOfFire() {
        if(this.getHealth()==true && (this.getSpecialStatus().equalsIgnoreCase(""))) return true;
        else if (this.getSpecialStatus().equalsIgnoreCase("laying eggs")) return false;
        else return false;
    }
}