package Animal;
import Animal.Animal;
public class Snake extends Reptil {
    private String specialStatus;
    
    public Snake(String nama, int id, double length, double weight, String gender, String type, String health){
        super(nama, id, length, weight, gender, type, health);
        this.specialStatus = "";
    }
    public Snake(String nama, int id, double length, double weight, String gender, String type,String specialStatus, String health){
        super(nama, id, length, weight, gender, type, specialStatus, health);
    }
    public boolean dancingAnimal() {
        if (this.getHealth()==true && (this.getSpecialStatus().equalsIgnoreCase("tame") || this.getSpecialStatus().equals(""))) return true;
        else return false;
    }
    public boolean passionateCoders() {
        if (this.getHealth()==true && (this.getSpecialStatus().equalsIgnoreCase("tame") || this.getSpecialStatus().equals(""))) return true;
        else return false;
    }
}