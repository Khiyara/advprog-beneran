package Attraction;

public interface Attraction {
    boolean circleOfFire();
    
    boolean countingMaster();
    
    boolean dancingAnimal();
    
    boolean passionateCoders();
}
