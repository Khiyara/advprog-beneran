
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import Animal.*;
public class CSVReader {
    public static int cat = 0; 
    public static int hamster = 1; 
    public static int snake = 6; 
    public static int eagle = 4; 
    public static int parrot = 5; 
    public static int whale = 3;
    public static int lion = 2;
    
    public static void readAnimalRecord(File file) { //Membaca animals_records.csv membaut objek hewan dan memasukkan keArrayList hewan sekalian mencek validasi
        try {
            Scanner inputStream = new Scanner(file);
            while(inputStream.hasNextLine()) {
                String data = inputStream.nextLine();
                String[] pisah = data.split(",");
                try {
                    if(pisah[1].equals("Hamster")) {
                        A3Festival.animalList.get(hamster).add(new Hamster(pisah[2],Integer.parseInt(pisah[0]),Double.parseDouble(pisah[4]),Double.parseDouble(pisah[5]),pisah[3],"Hamster",pisah[6],pisah[7]));
                        A3Festival.adaAnimals[hamster] = true;
                        A3Festival.valid++;
                    }
                    else if(pisah[1].equals("Lion")) {
                        A3Festival.animalList.get(lion).add(new Lion(pisah[2],Integer.parseInt(pisah[0]),Double.parseDouble(pisah[4]),Double.parseDouble(pisah[5]),pisah[3],"Lion",pisah[6],pisah[7]));
                        A3Festival.adaAnimals[lion] = true;
                        A3Festival.valid++;
                    }
                    else if(pisah[1].equals("Eagle")) {
                        A3Festival.animalList.get(eagle).add(new Eagle(pisah[2],Integer.parseInt(pisah[0]),Double.parseDouble(pisah[4]),Double.parseDouble(pisah[5]),pisah[3],"Eagle",pisah[6],pisah[7]));
                        A3Festival.adaAnimals[eagle] = true;
                        A3Festival.valid++;
                    }
                    else if(pisah[1].equals("Cat")) {
                        A3Festival.animalList.get(cat).add(new Cat(pisah[2],Integer.parseInt(pisah[0]),Double.parseDouble(pisah[4]),Double.parseDouble(pisah[5]),pisah[3],"Cat",pisah[6],pisah[7]));
                        A3Festival.adaAnimals[cat] = true;
                        A3Festival.valid++;
                    }
                    else if(pisah[1].equals("Parrot")) {
                        A3Festival.animalList.get(parrot).add(new Parrot(pisah[2],Integer.parseInt(pisah[0]),Double.parseDouble(pisah[4]),Double.parseDouble(pisah[5]),pisah[3],"Parrot",pisah[6],pisah[7]));
                        A3Festival.adaAnimals[parrot] = true;
                        A3Festival.valid++;
                    }
                    else if(pisah[1].equals("Snake")) {
                        A3Festival.animalList.get(snake).add(new Snake(pisah[2],Integer.parseInt(pisah[0]),Double.parseDouble(pisah[4]),Double.parseDouble(pisah[5]),pisah[3],"Snake",pisah[6],pisah[7]));
                        A3Festival.adaAnimals[snake] = true;
                        A3Festival.valid++;
                    }
                    else if(pisah[1].equals("Whale")) {
                        A3Festival.animalList.get(whale).add(new Whale(pisah[2],Integer.parseInt(pisah[0]),Double.parseDouble(pisah[4]),Double.parseDouble(pisah[5]),pisah[3],"Whale",pisah[6],pisah[7]));
                        A3Festival.adaAnimals[whale] = true;
                        A3Festival.valid++;
                    } else {
                        A3Festival.error++;
                    }
                } catch (Exception e) {
                    System.out.println("Error wrong format");
                }
            }
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static void readAnimalAttraction(File file){ //membaca animals_attractions.csv dan memvalidasi hewan apakah bisa melakukan attraksi tersebut
        int cof = 0; int da = 1; int cm = 2; int pc = 3;
        A3Festival.listAttraction = new boolean[7][4];
        
        int correctAtt = 0;
        try {
            Scanner inputStream = new Scanner(file);
            inputStream.useDelimiter(",");
            while(inputStream.hasNextLine()) {
                String data = inputStream.nextLine();
                String[] pisah = data.split(",");
                if(pisah[0].equals("Whale")) {
                    if(pisah[1].equalsIgnoreCase("Circles of Fires")) {
                        A3Festival.listAttraction[whale][cof] = true;
                        A3Festival.canDoAttraction[whale] = true;
                    }
                    else if(pisah[1].equalsIgnoreCase("Counting Masters")) {
                        A3Festival.listAttraction[whale][cm] = true;
                        A3Festival.canDoAttraction[whale] = true;
                    }
                } else if(pisah[0].equals("Lion")) {
                    if(pisah[1].equalsIgnoreCase("Circles of Fires")) {
                        A3Festival.listAttraction[lion][cof] = true;
                        A3Festival.canDoAttraction[lion] = true;
                    }
                } else if(pisah[0].equals("Eagle")) {
                    if(pisah[1].equalsIgnoreCase("Circles of Fires")) {
                        A3Festival.listAttraction[eagle][cof] = true;
                        A3Festival.canDoAttraction[eagle] = true;
                    }
                } else if(pisah[0].equals("Snake")) {
                    if(pisah[1].equalsIgnoreCase("Dancing Animals")) {
                        A3Festival.listAttraction[snake][da] = true;
                        A3Festival.canDoAttraction[snake] = true;
                    }
                    else if(pisah[1].equalsIgnoreCase("Passionate Coders")) {
                        A3Festival.listAttraction[snake][pc] = true;
                        A3Festival.canDoAttraction[snake] = true;
                    }
                } else if(pisah[0].equals("Parrot")) {
                    if(pisah[1].equalsIgnoreCase("Dancing Animals")) {
                        A3Festival.listAttraction[parrot][da] = true;
                        A3Festival.canDoAttraction[parrot] = true;
                    }
                    else if(pisah[1].equalsIgnoreCase("Counting Masters")) {
                        A3Festival.listAttraction[parrot][cm] = true;
                        A3Festival.canDoAttraction[parrot] = true;
                    }
                } else if(pisah[0].equals("Cat")) {
                    if(pisah[1].equalsIgnoreCase("Dancing Animals")) {
                        A3Festival.listAttraction[cat][da] = true;
                        A3Festival.canDoAttraction[cat] = true;
                    }
                    else if(pisah[1].equalsIgnoreCase("Passionate Coders")) {
                        A3Festival.listAttraction[cat][pc] = true;
                        A3Festival.canDoAttraction[cat] = true;
                    }
                } else if(pisah[0].equals("Hamster")) {
                    if(pisah[1].equalsIgnoreCase("Dancing Animals")) {
                        A3Festival.listAttraction[hamster][da] = true;
                        A3Festival.canDoAttraction[hamster] = true;
                    }
                    else if(pisah[1].equalsIgnoreCase("Passionate Coders")) {
                        A3Festival.listAttraction[hamster][pc] = true;
                        A3Festival.canDoAttraction[hamster] = true;
                    }
                    else if(pisah[1].equalsIgnoreCase("Counting Masters")) {
                        A3Festival.listAttraction[hamster][cm] = true;
                        A3Festival.canDoAttraction[hamster] = true;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    
    }
    public static void readAnimalSection(File file) {  //membaca animals_categories.csv apakah hewan tersebut sudah benar dikelompokan berdasarkan mamal aves reptil
        try {
            Scanner inputStream = new Scanner(file);
            inputStream.useDelimiter(",");
            while(inputStream.hasNextLine()) {
                String data = inputStream.nextLine();
                String[] pisah = data.split(",");
                if((pisah[0].equals("Hamster") || pisah[0].equals("Cat") || pisah[0].equals("Lion") || pisah[0].equals("Whale")) && pisah[1].equals("mammals") && pisah[2].equals("Explore the Mammals")) A3Festival.correctSection[0] = true;
                else if((pisah[0].equals("Eagle") || pisah[0].equals("Parrot")) && pisah[1].equals("aves") && pisah[2].equals("World of Aves")) A3Festival.correctSection[1] = true;
                else if(pisah[0].equals("Snake") && pisah[1].equals("reptiles") && pisah[2].equals("Reptillian Kingdom")) A3Festival.correctSection[2] = true;
                else {
                    A3Festival.errorSection++;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
