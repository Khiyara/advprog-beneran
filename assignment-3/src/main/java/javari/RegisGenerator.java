import java.nio.file.Path;
import java.nio.file.Paths;
import Animal.*;
import Regis.*;
import java.util.ArrayList;
import Attraction.SelectedAttraction;
public class RegisGenerator {
    public static void regisGenerator(int id, String nama, String attraction, ArrayList<Animal> listAnimal) { //Menambahkan atraksi ke Pendaftar selama Nama Pendaftar masih sama maka hanya menambahkan attraksi
        SelectedAttraction selected = new SelectedAttraction(attraction,listAnimal.get(0).getType());
        if(attraction.equalsIgnoreCase("Dancing Animals")) {
            for(Animal x : listAnimal) {
                if(x.dancingAnimal()) selected.addPerformer(x);
            }
        } else if(attraction.equalsIgnoreCase("Passionate Coders")) {
            for(Animal x : listAnimal) {
                if(x.passionateCoders()) selected.addPerformer(x);
            }
        } else if(attraction.equalsIgnoreCase("Counting Master")) {
            for(Animal x : listAnimal) {
                if(x.countingMaster()) selected.addPerformer(x);
            }
        }else if(attraction.equalsIgnoreCase("Circle of Fires")) {
            for(Animal x : listAnimal) {
                if(x.circleOfFire()) selected.addPerformer(x);
            }
        }
        boolean ada = false;
        if (A3Festival.listRegis.size()>0) {
            for(Regis x : A3Festival.listRegis) {
                if(x.getVisitorName().equalsIgnoreCase(nama)) {
                    ada = true;
                    x.addSelectedAttraction(selected);
                }
            }
        }
        if (!ada) { // Jika nama beda maka buat id baru dan ticket baru
            A3Festival.listRegis.add(new Regis(id, nama));
            A3Festival.listRegis.get(id-1).addSelectedAttraction(selected);   
            A3Festival.id++;
        }
    }
    public static void regisNow(int id){ //Membaut file JSON dengan memangil writeJson pada RegistrationWriter
        Path path = Paths.get(A3Festival.input);
        for (int i=0;i<id-1;i++) {
            try{
                RegistrationWriter.writeJson(A3Festival.listRegis.get(i), path);
            }catch (Exception e){
                System.out.println("Error gk nemu file");
            }
        }
        
    }
    public static void attractionGenerator(String attraction, ArrayList<Animal> animal, String nama) { //Mencetak nama hewan yang bisa melakukan attraksi
        String text = "Final Check!\nHere is your data, and the attraction you chose:\nName: "+nama+"\nAttraction: "+attraction+"->"+animal.get(0).getType()+"\nwith: ";
        if(attraction.equalsIgnoreCase("Dancing Animals")) {
            for (Animal x : animal) {
                if (x.dancingAnimal()) {
                    text += x.getNama()+",";
                }
            }
        } else if (attraction.equalsIgnoreCase("Passionate Coders")) {
            for (Animal x : animal) {
                if (x.passionateCoders()) {
                    text += x.getNama()+",";
                }
            }
        } else if (attraction.equalsIgnoreCase("Counting Master")) {
            for (Animal x : animal) {
                if (x.countingMaster()) {
                    text += x.getNama()+",";
                }
            }
        } else if (attraction.equalsIgnoreCase("Circle of Fires")) {
            for (Animal x : animal) {
                if (x.circleOfFire()) {
                    text += x.getNama()+",";
                }
            }
        }
        text += "\nIs the data correct ? (Y/N): ";
        System.out.print(text);
    }
}