import javax.swing.JButton;
/**
    Referensi:
    https://codereview.stackexchange.com/questions/85833/basic-memory-match-game-in-java
    Setiap kartu memiliki ID yang berbeda
    Oleh karena itu setiap kartu akan berbeda gambarnya
*/
public class Card extends JButton{
    private String id;
    public static String[] listImg = {"imageHS/1.png","imageHS/2.png","imageHS/3.png","imageHS/4.png","imageHS/5.png","imageHS/6.png","imageHS/7.png","imageHS/8.png","imageHS/9.png","imageHS/10.png","imageHS/11.png","imageHS/12.png","imageHS/13.png","imageHS/14.png","imageHS/15.png","imageHS/16.png","imageHS/17.png","imageHS/18.png"};
    private boolean matched = false;

    public void setId(int id){
        this.id = listImg[id];
    }
    public String getId(){
        return this.id;
    }
    public void setMatched(boolean matched){
        this.matched = matched;
    }

    public boolean getMatched(){
        return this.matched;
    }
}
